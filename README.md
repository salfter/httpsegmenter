httpsegmenter
=============

I grabbed this off of Google Code a few years ago; it was useful at the time
for streaming live audio to my iPhone (back when I was using an iPhone).  I
came up with a workflow that I documented in [this StackOverflow post](https://stackoverflow.com/a/36227445), 
and I put it to work in a live-stream timeshifter that converts radio
stations' streams into podcasts.

I just tried building it again, though, and the compile failed with some
undeclared variables or something.  A bit of digging indicated that (1)
something had changed in libavcodec that broke httpsegmenter and (2) ffmpeg
has gained the ability to create the files for HTTP live streaming all by
itself.

I made a few tweaks to the source of this program to get it to build again. 
They're in the latest commit, but are untested as of yet.  A better idea at
this point might be to use ffmpeg's built-in segmenter.  I got a stream
working pretty quickly with [this Medium post](https://medium.com/androvideo/convert-rtsp-to-hls-using-ffmpeg-2fe2cdf3a0de), 
and got it dialed in further (mainly to keep it from filling the disk) with
[this bit of ffmpeg documentation](https://www.ffmpeg.org/ffmpeg-formats.html#segment).

